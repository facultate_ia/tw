package com.example.ResourceManager.Repository;

import com.example.ResourceManager.Entity.Rights;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RightRepository extends JpaRepository<Rights, Integer> {
}
