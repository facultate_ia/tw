package com.example.ResourceManager.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Roles {
    @Id
    @Column(name = "id")
    private int id;

    @Column
    private String name;

    @ManyToMany(mappedBy = "roles")
    private Set<Users> users;

    @ManyToMany()
    private Set<Resources> resources;

    @ManyToMany()
    private Set<Rights> rights;

    public int getId() {
        return id;
    }

    public void setRoleId(int roleId) {
        this.id = roleId;
    }

    public String getName() {
        return name;
    }

    public void setRoleName(String roleName) {
        this.name = roleName;
    }

    public Set<Users> getUsers() {
        return users;
    }

    public void setUsers(Set<Users> users) {
        this.users = users;
    }

    public Set<Resources> getResources() {
        return resources;
    }

    public void setResources(Set<Resources> resources) {
        this.resources = resources;
    }

    public Set<Rights> getRights() {
        return rights;
    }

    public void setRights(Set<Rights> rights) {
        this.rights = rights;
    }
}
