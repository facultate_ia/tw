package com.example.ResourceManager.Entity;

import javax.persistence.*;
import java.util.*;

@Entity
public class Users {
    @Id
    @Column(name = "id")
    private int id;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String type;

    @ManyToMany()
    private Set<Roles> roles;

    public Users() {

    }

    public Users(String username) {
        this.username = username;
    }

    public int getUserId() {
        return id;
    }

    public Set<Roles> getRoles() {
        return roles;
    }

    public List<Roles> getSortedRoles() {
        List<Roles> roles = new ArrayList<Roles>();
        roles.addAll(this.roles);
        roles.sort((o1, o2) -> {return Integer.compare(o1.getId(), o2.getId());});
        return roles;
    }

    public List<String> getUniqueSortedRoles() {
        List<String> roles = new ArrayList<>();
        for (Roles role : this.roles) {
            if (!roles.contains(role.getName())) {
                roles.add(role.getName());
            }
        }
        roles.sort((o1, o2) -> {return o1.compareTo(o2);});
        return roles;
    }

    public void setRoles(Set<Roles> roles) {
        this.roles = roles;
    }

    public void setUserId(int userId) {
        this.id = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return type;
    }

    public void setUserType(String userType) {
        this.type = userType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users users = (Users) o;
        return Objects.equals(username, users.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
