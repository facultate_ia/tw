package com.example.ResourceManager.Controller;

import com.example.ResourceManager.Entity.Resources;
import com.example.ResourceManager.Entity.Rights;
import com.example.ResourceManager.Entity.Roles;
import com.example.ResourceManager.Entity.Users;
import com.example.ResourceManager.Repository.ResourceRepository;
import com.example.ResourceManager.Repository.RightRepository;
import com.example.ResourceManager.Repository.RoleRepository;
import com.example.ResourceManager.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@Controller
public class UserHomeController {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ResourceRepository resourceRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RightRepository rightRepository;

    private String getCurrentUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getName();
    }

    private Users getCurrentUser() {
        String username = this.getCurrentUsername();
        for (Users user : userRepository.findAll()) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

    private List<List> getCurrentUserRoles() {
        String username = getCurrentUsername();

        List<Roles> roles = (List<Roles>) roleRepository.findAll();
        List<Roles> currentRoles = new ArrayList<Roles>();

        List<Users> users = userRepository.findAll();

        for (Users user : users) {
            if (user.getUsername().equals(username)) {
                currentRoles.addAll(user.getRoles());
            }
        }

        List<List> rolesDisplay = new ArrayList<>();
        for (Roles role : currentRoles) {
            rolesDisplay.add(new ArrayList<>());

            int lastIndex = rolesDisplay.size() - 1;
            rolesDisplay.get(lastIndex).add(role);
            rolesDisplay.get(lastIndex).add(role.getRights().toArray()[0]);

            if (role.getResources().toArray().length > 0){
                rolesDisplay.get(lastIndex).add(role.getResources().toArray()[0]);
            }
            else {
                rolesDisplay.get(lastIndex).add(null);
            }
        }

        return rolesDisplay;
    }

    private List<String> getUserResources(List<List> rolesDisplay) {
        List<String> resourcesStrings = new ArrayList<>();

        for (List row : rolesDisplay) {
            Resources res = null;
            if (row.get(2) != null) {
                res = (Resources)row.get(2);

                if (!resourcesStrings.contains(res.getName())) {
                    resourcesStrings.add(res.getName());
                }
            }
        }

        return resourcesStrings;
    }

    @RequestMapping("user/home")
    public String displayRoles(Model model) {
        List<List> rolesDisplay = getCurrentUserRoles();
        model.addAttribute("userRoles", rolesDisplay);

        List<String> userResources = getUserResources(rolesDisplay);
        userResources.sort(Comparator.naturalOrder());
        model.addAttribute("userResources", userResources);

        Map<String, Boolean> rights = new HashMap<>();
        rights.put("Read", false);
        rights.put("Write", false);
        rights.put("Update", false);
        rights.put("Delete", false);
        model.addAttribute("rights", rights);

        //model.addAttribute("currentUser", userRepository.findByUsername())

        return "user/home";
    }

    @RequestMapping("/selectResource/{resourceName}")
    public String selectResource(@PathVariable String resourceName, Model model) {
        List<List> currentUserRoles = getCurrentUserRoles();
        Map<String, Boolean> rights = new HashMap<>();
        rights.put("Read", false);
        rights.put("Write", false);
        rights.put("Update", false);
        rights.put("Delete", false);

        Resources currentResource = null;
        for (List row : currentUserRoles) {
            Rights right = (Rights)row.get(1);
            Resources res = null;
            if (row.get(2) != null) {
                res = (Resources)row.get(2);

                if (res.getName().equals(resourceName)) {
                    rights.put(right.getType(), true);
                    currentResource = res;
                }
            }
        }

        displayRoles(model);
        model.addAttribute("rights", rights);
        model.addAttribute("mainInputText", currentResource.getValue());
        model.addAttribute("currentResource", currentResource);
        model.addAttribute("currentRole", currentResource.getRoles().toArray()[0]);

        return "user/home";
    }

    @RequestMapping("/saveResource/{currentResourceId}")
    public String saveResource(
            @RequestParam(value="mainInput", required = false) String mainInput,
            @PathVariable Integer currentResourceId,
            Model model) {

        Resources newResource =  resourceRepository.findById(currentResourceId).get();
        newResource.setValue(mainInput);
        resourceRepository.save(newResource);

        displayRoles(model);
        Map<String, Boolean> rights = new HashMap<>();
        rights.put("Read", false);
        rights.put("Write", false);
        rights.put("Update", false);
        rights.put("Delete", false);
        model.addAttribute("rights", rights);

        return "user/home";
    }

    @RequestMapping("/deleteResource/{currentResourceName}")
    public String deleteResource(@PathVariable String currentResourceName, Model model) {
        String username = getCurrentUsername();

        //find current user
        Users currentUser = userRepository.findByUsername(username);

        Resources currentResource = resourceRepository.findByName(currentResourceName);
        Roles currentRole = (Roles) currentResource.getRoles().toArray()[0];
        Rights currentRight = (Rights) currentRole.getRights().toArray()[0];

        if (currentUser.getRoles().contains(currentRole)
                && currentRight.getType().equals("Delete")) {
            currentRole.getResources().remove(currentResource);
            userRepository.save(currentUser);
        }
        else {
            model.addAttribute("warningMessage", "Error! Someone changed your current rights on this resources and you can no longer delete the current resource!");
        }

        displayRoles(model);
        Map<String, Boolean> rights = new HashMap<>();
        rights.put("Read", false);
        rights.put("Write", false);
        rights.put("Update", false);
        rights.put("Delete", false);
        model.addAttribute("rights", rights);

        return "user/home";
    }
}
