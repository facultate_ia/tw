package com.example.ResourceManager.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Rights {
    @Id
    @Column(name = "id")
    private int id;

    @Column
    private String type;

    @ManyToMany(mappedBy = "rights")
    private Set<Roles> roles;

    public Set<Roles> getRoles() {
        return roles;
    }

    public void setRoles(Set<Roles> roles) {
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public void setId(int rightId) {
        this.id = rightId;
    }

    public String getType() {
        return type;
    }

    public void setType(String rightType) {
        this.type = rightType;
    }
}
