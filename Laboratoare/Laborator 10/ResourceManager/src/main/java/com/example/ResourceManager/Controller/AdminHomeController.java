package com.example.ResourceManager.Controller;

import com.example.ResourceManager.Entity.Roles;
import com.example.ResourceManager.Entity.Users;
import com.example.ResourceManager.Repository.ResourceRepository;
import com.example.ResourceManager.Repository.RightRepository;
import com.example.ResourceManager.Repository.RoleRepository;
import com.example.ResourceManager.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class AdminHomeController {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ResourceRepository resourceRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RightRepository rightRepository;

    private Map<String, List<List>> lastState;

    private void initPage(Model model) {
        model.addAttribute("roles", roleRepository.findAll());
        model.addAttribute("users", userRepository.findAll());
    }

    @RequestMapping("admin/home")
    public String listResources(Model model) {
        initPage(model);
        return "admin/home";
    }

    @RequestMapping("/selectUserRemove")
    public String selectUser(
            @RequestParam(value="userItem", required = false) String userItem,
            Model model) {
        initPage(model);

        Users user = userRepository.findByUsername(userItem);
        List<String> currentUserRoles = user.getUniqueSortedRoles();
        model.addAttribute("currentUserRoles", currentUserRoles);
        model.addAttribute("currentUser", user.getUsername());

        List<String> unassignedRoles = new ArrayList<>();
        for (Roles role : roleRepository.findAll()) {
            if (!unassignedRoles.contains(role.getName())
                    && !currentUserRoles.contains(role.getName())) {
                unassignedRoles.add(role.getName());
            }
        }
        model.addAttribute("currentUserUnassignedRoles", unassignedRoles);

        return "admin/home";
    }

    @RequestMapping("/selectRoleRemove/{currentUser}")
    public String selectRoleRemove(
            @RequestParam(value="roleItem", required = false) String roleItem,
            @PathVariable String currentUser,
            Model model) {

        Users user = userRepository.findByUsername(currentUser);
        List<Roles> roles = roleRepository.findAllByName(roleItem);

        for (Roles role : roles) {
            user.getRoles().remove(role);
        }
        userRepository.save(user);

        initPage(model);
        return "admin/home";
    }

    @RequestMapping("/assignRole/{currentUser}")
    public String assignRole(
            @RequestParam(value="roleItem", required = false) String roleItem,
            @PathVariable String currentUser,
            Model model) {

        Users user = userRepository.findByUsername(currentUser);
        List<Roles> roles = roleRepository.findAllByName(roleItem);

        for (Roles role : roles) {
            user.getRoles().add(role);
        }
        userRepository.save(user);

        initPage(model);
        return "admin/home";
    }
}

