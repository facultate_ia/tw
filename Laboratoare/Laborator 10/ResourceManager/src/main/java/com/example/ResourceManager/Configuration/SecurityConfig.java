package com.example.ResourceManager.Configuration;

import com.example.ResourceManager.Entity.Users;
import com.example.ResourceManager.Handler.UrlAuthenticationSuccessHandler;
import com.example.ResourceManager.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
    class SecurityConfig extends WebSecurityConfigurerAdapter {

        @Autowired
        UserRepository userRepository;

        @Bean
        public AuthenticationSuccessHandler myAuthenticationSuccessHandler(){
            return new UrlAuthenticationSuccessHandler();
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            //@formatter:off
            http
                .authorizeRequests()
                    .antMatchers("/css/**").permitAll()
                    .antMatchers("/login*").permitAll()
                    .antMatchers("/home").permitAll() //.access("hasRole('USER') or hasRole('ADMIN')")
                    .antMatchers("/user/home").access("hasRole('USER')")
                    .antMatchers("/admin/home").access("hasRole('ADMIN')")
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    //.loginPage("/login")
                    //.defaultSuccessUrl("/home")

                    .loginPage("/login")
                    .loginProcessingUrl("/login")
                    .successHandler(myAuthenticationSuccessHandler())

                    //.permitAll()
                    .and()
                .logout()
                    .logoutUrl("/logout")
                    .permitAll();
            //@formatter:on
        }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        for (Users user : userRepository.findAll()) {
            String username = user.getUsername();
            String password = user.getPassword();
            String role = user.getUserType();

            auth.inMemoryAuthentication().withUser(username).password("{noop}" + password).roles(role);
        }
    }
}
