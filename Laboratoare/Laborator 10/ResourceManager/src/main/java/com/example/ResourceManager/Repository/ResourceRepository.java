package com.example.ResourceManager.Repository;

import com.example.ResourceManager.Entity.Resources;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceRepository extends CrudRepository<Resources, Integer> {
    Integer deleteByName(String name);
    Resources findByName(String name);
}