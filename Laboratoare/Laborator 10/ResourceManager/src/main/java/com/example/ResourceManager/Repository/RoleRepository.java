package com.example.ResourceManager.Repository;

import com.example.ResourceManager.Entity.Roles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends CrudRepository<Roles, Integer> {
    Integer deleteByName(String name);
    List<Roles> findAllByName(String name);
}
