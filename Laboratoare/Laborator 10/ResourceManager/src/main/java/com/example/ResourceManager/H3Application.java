package com.example.ResourceManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.example.ResourceManager"})
@EntityScan("com.example.ResourceManager")
@EnableJpaRepositories("com.example.ResourceManager.Repository")
public class H3Application {

    public static void main(String[] args) {
        SpringApplication.run(H3Application.class, args);
    }

}
