package com.example.ResourceManager.Entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Resources {
    @Id
    @Column(name = "id")
    private int id;

    @Column
    private String name;

    @Column
    private String value;

    @ManyToMany(mappedBy = "resources")
    private Set<Roles> roles;

    public Set<Roles> getRoles() {
        return roles;
    }

    public void setRoles(Set<Roles> roles) {
        this.roles = roles;
    }

    public int getId() {
        return id;
    }

    public void setId(int resourceId) {
        this.id = resourceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String resourceName) {
        this.name = resourceName;
    }

    public String getValue() { return value; }

    public void setValue(String value) { this.value = value; }
}
