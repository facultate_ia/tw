import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/* If we type here the optional attribute "urlPatterns"
we do not need anymore to write the same data in web.xml (leave the web.xml clean)*/
@WebServlet(name = "MyServlet"
        //,urlPatterns = {"/login"}
)
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String isRemember = request.getParameter("remember");

        if (email.equals("lucian.ritan@gmail.com") && password.equals("qwe123")) {
            response.setContentType("text/html");
            request.setAttribute("user", email);
            RequestDispatcher req = request.getRequestDispatcher("success");
            req.forward(request, response);
        } else {
            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
            rd.include(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
