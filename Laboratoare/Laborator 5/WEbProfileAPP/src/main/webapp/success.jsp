<%--
  Created by IntelliJ IDEA.
  User: PC
  Date: 11/12/2020
  Time: 5:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Logged In</title>
    <!-- Bootstrap design -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title">Logged in</h5>
            <p class="card-text">
            <% String username = (String) request.getAttribute("user"); %>
            Welcome <% out.println(username); %> !!!! You have logged in.
            </p>
            <a href="#" class="btn btn-primary">Exit</a>
        </div>
    </div>
</body>
