package com.collection;

import java.util.HashSet;
import java.util.Set;

public class SetsExamples {
    public static void exmaples() {
        System.out.println("\n========================= SETS =========================");

        Set<String> set1 = new HashSet<>();
        set1.add("Apple");
        set1.add("Orange");
        set1.add("Mango");
        set1.add("Pineapple");
        set1.add("Pear");
        set1.add("Mango");
        set1.add("Orange");
        System.out.println("An initial list of elements: " + set1);
        // Remove on specific element
        set1.remove("Pear");
        System.out.println("The new list of elements: " + set1);

        Set<String> set2 = new HashSet<>();
        set2.add("Plum");
        set2.add("Dragon Fruit");
        set2.add("Pear");
        set1.add("Orange");

        set1.addAll(set2);

        //Removing elements on the basis of specified condition
        set1.removeIf(str -> str.contains("Mango"));
        System.out.println("After invoking removeIf() method: " + set1);
    }
}
