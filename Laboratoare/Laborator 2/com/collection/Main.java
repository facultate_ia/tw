package com.collection;
import com.streams.Streams;

public class Main {

    public static void main(String[] args) {
        ArraysExamples.examples();
        MapsExamples.exmaples();
        SetsExamples.exmaples();
        Streams.examples();
    }
}