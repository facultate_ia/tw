package com.collection;

import com.enums.Roles;
import com.model.Person;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapsExamples {
    //Map<Key, Value>
    public static void exmaples() {
        System.out.println("\n========================= MAPS =========================");
        Person person1 = new Person("Andrei", "198021222081231", 23);
        Person person2 = new Person("Ana", "297021222083232", 22);
        Person person3 = new Person("Maria", "420021222083232", 19);
        Person person4 = new Person("Mihai", "321021222081234", 18);

        HashMap<Person, Roles> roles = new HashMap<Person, Roles>();

        roles.put(person1, Roles.USER);
        roles.put(person1, Roles.ADMIN);
        roles.put(person2, Roles.ADMIN);
        roles.put(person3, Roles.MODERATOR);
        roles.put(person4, Roles.USER);

        System.out.println("This is my map " + roles);

        //Iterate with an Iterator
        System.out.println("\nIterate with Iterator:");

        Iterator it = roles.entrySet().iterator();
        while (it.hasNext()){
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair);
        }

        //Iterate the map - lambda
        System.out.println("\nIterate with lambda function:");

        roles.forEach((key, value) -> {
            System.out.println(key + "=" + value);
        });
    }
}
