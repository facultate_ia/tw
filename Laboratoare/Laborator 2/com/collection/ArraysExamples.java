package com.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArraysExamples {
    public static void examples() {
        System.out.println("\n========================= ARRAYS =========================");
        //Lists<E>
        //Arrays
        //Init the array via constructor
        List<Integer> arrayList = new ArrayList<Integer>(Arrays.asList(2, 3, 1, 4, 10, 0));

        //CRUD operations
        //Create
        arrayList.add(25);
        arrayList.add(30);
        arrayList.add(20);

        //Read all elements
        System.out.println("Initial array :" + arrayList);
        //Read an element
        System.out.println("Element from index 1: " + arrayList.get(1));

        //Update
        arrayList.set(0, 0); // update the element from first position to value zero

        //Delete
        arrayList.remove(8);

        System.out.println("Array Updated :" + arrayList);

        //Arrays class functions
        arrayList.sort(Integer::compareTo);

        System.out.println("Array sorted asc:" + arrayList);

        //Reverse the array using Collections and lambda expresion
        Collections.sort(arrayList, (first, second) -> {
            if (first > second)
                return -1;
            else return 0;
        });

        System.out.println("Array sorted desc:" + arrayList);

    }
}
