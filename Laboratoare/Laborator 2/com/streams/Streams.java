package com.streams;

import com.enums.Gender;
import com.model.Person;

import java.util.*;
import java.util.stream.Collectors;

public class Streams {
    public static void examples() {
        System.out.println("\n========================= STREAMS =========================");

        List<Person> people = getPeople();
        System.out.println("Initial list:" + people);

        //Imperative approach of a filter
        List<Person> males = new ArrayList<>();
        for (Person person : people) {
            if (person.getGender().equals(Gender.MALE)) {
                males.add(person);
            }
        }

        //Afisam prin lambda function
        System.out.println();
        males.forEach(System.out::println);

        System.out.println();
        males.forEach((man) -> {
            //For multiline code block
            System.out.println("- " + man);
        });

        //And now the declarative approach for the same iteration
        //Streams allows us to ask what we want, instead of defining the implementation as above
        List<Person> females = people.stream()
                .filter(person -> person.getGender().equals(Gender.FEMALE))
                .collect(Collectors.toList());

        System.out.println();
        females.forEach(System.out::println);

        //Sort
        List<Person> peopleSortedByAge = people.stream()
                /* If your Person class have the compareTo function implemented you can just write
                 *   sorted() */
                .sorted(Comparator.comparing(Person::getAge)/*.reversed() // for the reversed order*/)
                .collect(Collectors.toList());
        peopleSortedByAge.forEach(System.out::println);

        //All match
        final int minimumAgeRequired = 18;
        boolean allMatch = people.stream().
                allMatch(person -> person.getAge() >= minimumAgeRequired);

        System.out.println("\nAll people have over 18?: " + allMatch);

        //Max
        Optional<Person> optionalPerson = people.stream().max(Comparator.comparing(Person::getAge));
        System.out.println("Oldest person " + optionalPerson.get());

        //Group
        Map<Gender, List<Person>> genderGroup = people.stream().collect(Collectors.groupingBy(Person::getGender));

        System.out.println("\nGroup by gender:");
        genderGroup.forEach((gender, allPersons) -> {
            System.out.println(gender);
            allPersons.forEach(person -> System.out.println(person.getName()));
            System.out.println();
        });

        //Any match
        //Min
    }

    private static List<Person> getPeople() {
        return Arrays.asList(
                new Person("Andrei", "198021222081231", 23),
                new Person("Ana", "297021222083232", 22),
                new Person("Maria", "420021222083232", 19),
                new Person("Mihai", "321021222081234", 18)
        );
    }

}
