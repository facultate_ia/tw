package com.model;

import com.enums.Gender;

public class Person /*implements Comparable<Person>*/ {
    String name;
    String tin;
    Integer age;
    Gender gender;

    public Person(String name, String tin, Integer age) {
        this.name = name;
        this.tin = tin;
        this.age = age;

        final char firstCharacter = tin.charAt(0);
        if (firstCharacter == '1' || firstCharacter == '3' || firstCharacter == '5') {
            this.gender = Gender.MALE;
        } else {
            this.gender = Gender.FEMALE;
        }

    }

    public Person(String name, String tin, Integer age, Gender gender) {
        this.name = name;
        this.tin = tin;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                "gender='" + gender + '\'' +
                "age='" + age + '\'' +
                '}';
    }

/*    @Override
    public int compareTo(Person person) {
        return this.age.compareTo(person.age);
    }*/
}
