package com.company.streams;

import com.company.enums.Gender;
import com.company.model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Streams {
    public static void examples() {
        System.out.println("===============Streams===============");

        List<Person> people = getPeople();
        System.out.println("Initial list of people " + people);

        //Imperative approach of filter
        List<Person> males = new ArrayList<>();
        for(Person person : people) {
            if(person.getGender().equals(Gender.MALE)) {
                males.add(person);
            }
        }

        //Print with lambda
        System.out.println();
        males.forEach(System.out::println);

        System.out.println();
        males.forEach((man) -> {
            System.out.println("- " + man);
        });

        //Declarative approach
        List<Person> females = people.stream()
                .filter(person -> person.getGender().equals(Gender.FEMALE))
                .collect(Collectors.toList());Collectors.toList();

        System.out.println();
        females.forEach(System.out::println);

        //Sort
        List<Person> peopleSortedByAge = people.stream()
                .sorted(Comparator.comparing(Person::getAge))
                .collect(Collectors.toList());

        peopleSortedByAge = people.stream()
                .sorted()
                .collect(Collectors.toList());

        System.out.println(peopleSortedByAge);

        //All match
        final int minimumAgeRequired = 18;
        boolean b = people.stream()
                .allMatch(person -> person.getAge() >= minimumAgeRequired);
    }

    private static List<Person> getPeople() {
        return Arrays.asList(
                new Person("Andrei", 23, "1980213081234", Gender.MALE),
                new Person("Ana", 22, "1980213081234", Gender.FEMALE),
                new Person("Maria", 19, "6980213081234", Gender.FEMALE),
                new Person("Dragos", 23, "1980213081234", Gender.MALE)
        );
    }
}
