package com.company.collection;

import com.company.enums.Gender;
import com.company.enums.Role;
import com.company.model.Person;

import javax.swing.text.html.HTMLDocument;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MapExamples {

    public static void examples() {
        System.out.println("===============Maps===============");

        Person person1 = new Person("Andrei", 23, "1980213081234", Gender.MALE);
        Person person2 = new Person("Ana", 22, "1980213081234", Gender.FEMALE);
        Person person3 = new Person("Maria", 19, "6980213081234", Gender.FEMALE);
        Person person4 = new Person("Dragos", 23, "1980213081234", Gender.MALE);

        HashMap<Person, Role> roles = new HashMap<>();

        roles.put(person1, Role.ADMIN);
        roles.put(person2, Role.USER);
        roles.put(person3, Role.MODERATOR);
        roles.put(person4, Role.ADMIN);

        System.out.println("Map:" + roles);

        //Iterate with an Iterator
        System.out.println("\n Iterate with Iterator: ");
        Iterator it = roles.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println(pair);
        }

        //Iterate the map - lambda
        System.out.println("\n Iterate with lambda function: ");
        roles.forEach((key, value) -> {
            System.out.println(key + "=" + value);
        });
    }
}
