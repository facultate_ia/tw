package com.company.collection;

import java.util.HashSet;
import java.util.Set;

public class SetExamples {
    public static void examples() {
        System.out.println("===============Sets===============");

        Set<String> set1 = new HashSet<>();

        set1.add("Apple");
        set1.add("Orange");
        set1.add("Mango");
        set1.add("Pineapple");
        set1.add("Pear");
        set1.add("Orange");
        set1.add("Mango");

        System.out.println("Set's elements: " + set1);

        Set<String> set2 = new HashSet<>();

        set2.add("Plum");
        set2.add("Dragon Fruit");
        set2.add("Pear");
        set2.add("Orange");

        set1.addAll(set2);

        set1.removeIf(str -> str.contains("Mango"));

        System.out.println(set1);
    }
}
