package com.company.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArrayExamples {

    public static void examples() {
        // List<E>
        // Arrays
        System.out.println("===============Arrays===============");
        List<Integer> arrayList = new ArrayList<>(Arrays.asList(2, 3, 4, 10, 0));

        //CRUD operations
        //Create
        arrayList.add(25);
        arrayList.add(30);
        arrayList.add(20);

        //Read all elements
        System.out.println(arrayList);

        //Read an element
        System.out.println("Element at index 1:" + arrayList.get(1));

        //Update
        arrayList.set(0, 0);

        System.out.println("Array update: " + arrayList);

        //Sort
        arrayList.sort(Integer::compareTo);

        System.out.println("Sorted array: " + arrayList);

        //Reverse sort
        Collections.sort(arrayList, (first, second) -> {
            if (first > second) {
                return -1;
            } else {
                return 0;
            }
        });

        System.out.println("Array sorted desc:" + arrayList);
    }
}