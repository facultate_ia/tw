package com.company;

import com.company.collection.ArrayExamples;
import com.company.collection.MapExamples;
import com.company.collection.SetExamples;
import com.company.streams.Streams;

public class Main {

    public static void main(String[] args) {
        ArrayExamples.examples();
        MapExamples.examples();
        SetExamples.examples();
        Streams.examples();
    }
}
