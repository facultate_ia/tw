package com.company.model;

import com.company.enums.Gender;

public class Person implements Comparable<Person>{
    String name;
    Integer age;
    String tin;
    Gender gender;

    public Person(String name, Integer age, String tin, Gender gender) {
        this.name = name;
        this.age = age;
        this.tin = tin;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getTin() {
        return tin;
    }

    public void setTin(String tin) {
        this.tin = tin;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }

    @Override
    public int compareTo(Person person) {
        return this.age.compareTo(person.age);
    }
}
