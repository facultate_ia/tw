//import com.sun.xml.internal.bind.v2.runtime.reflect.Lister;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ServerThread extends Thread{
    private Socket socket = null;
    private ObjectInputStream in = null;
    private ObjectOutputStream out = null;

    public ServerThread(Socket socket) {
        this.socket = socket;
        try {
            //For receiving and sending data
            this.in = new ObjectInputStream(socket.getInputStream());
            this.out = new ObjectOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            /* Initial code
            Packet recivePacket = (Packet) this.in.readObject();
            System.out.println(recivePacket.message);

            if (recivePacket.message.equals("Hello")) {
                Packet packet = new Packet("Hello World");
                this.out.writeObject(packet);
            }
            */
            Packet recivePacket = (Packet) this.in.readObject();
            System.out.println("Recived: " + recivePacket.message);
            excute(recivePacket.message);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void excute(String message) {
        Packet packet = null;
        switch (message){
            case "Hello":
                packet = new Packet("Hello There");
                break;
            case "How are you?":
                packet = new Packet("I'm fine");
                break;
            case "Bye?":
                packet = new Packet("Bye");
                break;
            default:
                packet = new Packet("Can't understand you :/");
        }

        try {
            this.out.writeObject(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
