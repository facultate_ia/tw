--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-10-30 02:44:20

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 203 (class 1259 OID 16549)
-- Name: Student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Student" (
    first_name text,
    id integer NOT NULL,
    last_name text,
    age integer
);


ALTER TABLE public."Student" OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 16547)
-- Name: Student_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public."Student" ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Student_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 207 (class 1259 OID 16569)
-- Name: Subject; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Subject" (
    id integer NOT NULL,
    name text,
    id_teacher integer
);


ALTER TABLE public."Subject" OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16600)
-- Name: Subject_Student; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Subject_Student" (
    id_student integer NOT NULL,
    id_subject integer NOT NULL
);


ALTER TABLE public."Subject_Student" OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 16567)
-- Name: Subject_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public."Subject" ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Subject_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 205 (class 1259 OID 16559)
-- Name: Teacher; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Teacher" (
    id integer NOT NULL,
    first_name text,
    last_name text,
    age integer
);


ALTER TABLE public."Teacher" OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16557)
-- Name: Teacher_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public."Teacher" ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public."Teacher_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 2845 (class 0 OID 16549)
-- Dependencies: 203
-- Data for Name: Student; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Student" (first_name, id, last_name, age) FROM stdin;
Alex	1	Mincu\n	22
Marius	2	Ioniță	19
Vasile	3	Vasile	30
Vasile	4	Vasile	30
Vasile	5	Vasile	30
Vasile	6	Vasile	30
Alex	7	Mincu\n	22
Marius	8	Ioniță	19
\.


--
-- TOC entry 2849 (class 0 OID 16569)
-- Dependencies: 207
-- Data for Name: Subject; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Subject" (id, name, id_teacher) FROM stdin;
1	Sistem de operare	1
2	Retele de calculatoare	1
3	Algraf	2
\.


--
-- TOC entry 2850 (class 0 OID 16600)
-- Dependencies: 208
-- Data for Name: Subject_Student; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Subject_Student" (id_student, id_subject) FROM stdin;
1	2
1	1
1	3
2	3
2	1
\.


--
-- TOC entry 2847 (class 0 OID 16559)
-- Dependencies: 205
-- Data for Name: Teacher; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Teacher" (id, first_name, last_name, age) FROM stdin;
2	Alexandru	Dick	45
1	Ioan\n	Florea	60
\.


--
-- TOC entry 2856 (class 0 OID 0)
-- Dependencies: 202
-- Name: Student_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Student_id_seq"', 8, true);


--
-- TOC entry 2857 (class 0 OID 0)
-- Dependencies: 206
-- Name: Subject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Subject_id_seq"', 3, true);


--
-- TOC entry 2858 (class 0 OID 0)
-- Dependencies: 204
-- Name: Teacher_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Teacher_id_seq"', 2, true);


--
-- TOC entry 2707 (class 2606 OID 16556)
-- Name: Student Student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Student"
    ADD CONSTRAINT "Student_pkey" PRIMARY KEY (id);


--
-- TOC entry 2714 (class 2606 OID 16604)
-- Name: Subject_Student Subject_Student_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Subject_Student"
    ADD CONSTRAINT "Subject_Student_pkey" PRIMARY KEY (id_student, id_subject);


--
-- TOC entry 2711 (class 2606 OID 16576)
-- Name: Subject Subject_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Subject"
    ADD CONSTRAINT "Subject_pkey" PRIMARY KEY (id);


--
-- TOC entry 2709 (class 2606 OID 16566)
-- Name: Teacher Teacher_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Teacher"
    ADD CONSTRAINT "Teacher_pkey" PRIMARY KEY (id);


--
-- TOC entry 2712 (class 1259 OID 16582)
-- Name: fki_fk_teacher; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX fki_fk_teacher ON public."Subject" USING btree (id_teacher);


--
-- TOC entry 2716 (class 2606 OID 16605)
-- Name: Subject_Student fk_id_student; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Subject_Student"
    ADD CONSTRAINT fk_id_student FOREIGN KEY (id_student) REFERENCES public."Student"(id);


--
-- TOC entry 2717 (class 2606 OID 16610)
-- Name: Subject_Student fk_id_subject; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Subject_Student"
    ADD CONSTRAINT fk_id_subject FOREIGN KEY (id_student) REFERENCES public."Subject"(id);


--
-- TOC entry 2715 (class 2606 OID 16577)
-- Name: Subject fk_teacher; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Subject"
    ADD CONSTRAINT fk_teacher FOREIGN KEY (id_teacher) REFERENCES public."Teacher"(id);


-- Completed on 2020-10-30 02:44:20

--
-- PostgreSQL database dump complete
--

