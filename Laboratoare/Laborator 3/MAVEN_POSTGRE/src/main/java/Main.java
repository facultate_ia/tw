import database.Database;
import database.DatabaseConnection;
import model.Student;
import operations.StudentOperations;

public class Main {
    public static void main(String[] args)  {

        /*
         For  mysql driver
         "com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/your_db"

         Default port reserved for postgre is 5432
        */

        Database database = new Database("org.postgresql.Driver",
                "jdbc:postgresql://localhost:5432/College",
                "postgres", "toor");

        DatabaseConnection databaseConnection = new DatabaseConnection(database);
        StudentOperations studentOperations = new StudentOperations(databaseConnection);

        System.out.println("All students: ");
        studentOperations.getAll().forEach(System.out::println);

        Student student = studentOperations.get(2);
        System.out.println("Student with id 2: " + student);

        System.out.println();
        System.out.println(student);

        studentOperations.update(student, 8);
        System.out.println(studentOperations.get(8));

        System.out.println("Student 2 subjects:\n" + studentOperations.getSubjects(2));
    }
}
