package operations;

import database.DatabaseConnection;
import model.Student;
import model.Subject;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentOperations {
    private DatabaseConnection databaseConnection;

    public StudentOperations(DatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    /* CRUD */
    /* Create student */
    public void create(Student student) {
        databaseConnection.createConnection();
        String query = "INSERT INTO public.\"Student\" (first_name, last_name, age) VALUES (?, ?, ?)"; //? is a placeholder
        PreparedStatement ps = null;

        try {
            ps = databaseConnection.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, student.getFirstName());
            ps.setString(2, student.getLastName());
            ps.setInt(3, student.getAge());

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error when creating query: " + e.getMessage());
        } finally {
            try {
                ps.close();
                databaseConnection.getConnection().close();
            } catch (SQLException e) {
                System.err.println("Failed closing streams: " + e.getMessage());
            }
        }
    }

    /* Read a student by an id */
    public Student get(int id) {
        databaseConnection.createConnection();

        /*
        // Statment este vulnerabil la SQL Injection
        String query = String.format("SELECT * FROM public.\"Student\" WHERE id = %s", id);
        Statement statement = databaseConnection.getConnection().createStatement();
        statement.executeUpdate(query);*/

        //PrepareStatement este mai eficient și nu este vulnerabil la SQLInjection
        String query = String.format("SELECT * FROM public.\"Student\" WHERE id = %s", id);


        PreparedStatement ps = null;
        ResultSet rs = null;

        Student student = new Student();
        try {
            ps = databaseConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                student.setId(rs.getInt("id"));
                student.setFirstName(rs.getString("first_name"));
                student.setLastName(rs.getString("last_name"));
                student.setAge(rs.getInt("age"));
            }

        } catch (SQLException e) {
            System.err.println("Error when creating query: " + e.getMessage());
        }
        return student;
    }

    /*Get the students classes via a triple join */
    public List<Subject> getSubjects(int id) {
        databaseConnection.createConnection();
        String query = String.format("SELECT sub.id, sub.name " +
                "FROM \"Subject\" as sub " +
                "INNER JOIN \"Subject_Student\" as ss " +
                "ON sub.id = ss.id_subject " +
                "INNER JOIN \"Student\" as stu " +
                "ON ss.id_student = stu.id " +
                "WHERE stu.id = %s;", id);

        List<Subject> subjects = new ArrayList<>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = databaseConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Subject subject = new Subject();

                subject.setId(rs.getInt("id"));
                subject.setName(rs.getString("name"));
                subjects.add(subject);
            }
        }
        catch (SQLException e) {
            System.err.println("Error when creating query: " + e.getMessage());
        }
        return subjects;
    }

    /* Read all students */
    /* TODO Read all students with their subjects too */
    public List<Student> getAll() {
        databaseConnection.createConnection();
        String query = "SELECT * FROM public.\"Student\";";

        List<Student> students = new ArrayList<Student>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = databaseConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setFirstName(rs.getString("first_name"));
                student.setLastName(rs.getString("last_name"));
                student.setAge(rs.getInt("age"));

                students.add(student);
            }
        } catch (SQLException e) {
            System.err.println("Error when creating query: " + e.getMessage());
        }

        return students;
    }

    /* Update a student */
    public void update(Student student, int id) {
        databaseConnection.createConnection();
        String query = String.format("UPDATE public.\"Student\" SET first_name='%s', last_name='%s', age=%s WHERE id = %s;", student.getFirstName(), student.getLastName(), student.getAge(), id);

        PreparedStatement ps = null;
        try {
            ps = databaseConnection.getConnection().prepareStatement(query);
            ps.execute();
        } catch (SQLException e) {
            System.err.println("Error when creating query: " + e.getMessage());
        }

    }


}

