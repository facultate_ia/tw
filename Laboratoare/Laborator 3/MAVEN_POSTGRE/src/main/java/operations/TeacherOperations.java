package operations;

import database.DatabaseConnection;
import model.Teacher;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/* Exemplu și cu Entity Manager */
public class TeacherOperations {
    private DatabaseConnection databaseConnection;

    public TeacherOperations(DatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    public void create(Teacher teacher) {
        databaseConnection.createConnection();
        String query = "INSERT INTO public.\"Teacher\" (first_name, last_name, age) VALUES (?, ?, ?)"; //? is a placeholder
        PreparedStatement ps = null;

        try {
            ps = databaseConnection.getConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, teacher.getFirstName());
            ps.setString(2, teacher.getLastName());
            ps.setInt(3, teacher.getAge());

            ps.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Error when creating query: " + e.getMessage());
        } finally {
            try {
                ps.close();
                databaseConnection.getConnection().close();
            } catch (SQLException e) {
                System.err.println("Failed closing streams: " + e.getMessage());
            }
        }

    }

    public List<Teacher> getAll() {
        databaseConnection.createConnection();
        String query = "SELECT * FROM public.\"Teacher\";";

        List<Teacher> teachers = new ArrayList<Teacher>();

        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = databaseConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                //System.out.println(rs);
                Teacher teacher = new Teacher();
                teacher.setId(rs.getInt("id"));
                teacher.setFirstName(rs.getString("first_name"));
                teacher.setLastName(rs.getString("last_name"));
                teacher.setAge(rs.getInt("age"));

                teachers.add(teacher);
            }
        } catch (SQLException e) {
            System.err.println("Error when creating query: " + e.getMessage());
        }

        return teachers;
    }

}
