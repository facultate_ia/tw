package com.robertadrianbucur.WeatherApp;

import com.robertadrianbucur.WeatherApp.Models.Weather;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import java.util.Queue;

public class WeatherServlet extends HttpServlet {
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
    private ArrayList<Weather> weather24h = new ArrayList<>();
    private LocalTime now = LocalTime.now();

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        now = LocalTime.now();
        String nowFormatted = now.format(formatter);
        weather24h = new ArrayList<>();

        String city = request.getParameter("cities");
        getWeatherByCity(city);

        PrintWriter out = response.getWriter();
        out.println("<html><head><title>Weather</title></head><body>");
        out.println("<h2 style=\"text-align: center\"> Forecast for " + city + "</h2>");
        out.println("<h4 style=\"text-align: center\"> " + nowFormatted + "</h4>");
        out.println("<center><form method=\"get\" action=\"home\">");
        out.println("<button name=\"back\" type=\"back\" class=\"btn btn-primary\">Back</button></form></center>");
        out.println("<center><table style=\"width:50%\"><tr>");
        out.println("<th>Hour</th>");
        out.println("<th>Temperature °</th>");
        out.println("<th>Pressure mb</th>");
        out.println("<th>Rain Chances %</th></tr>");
        writeWeather(out);
        out.println("</table></center>");
        out.println("</body></html>");
    }

    private void getWeatherByCity(String city) {
        JSONParser parser = new JSONParser();
        try {
            File citiesFile = new File(Objects.requireNonNull(getClass().getClassLoader().getResource("weather.json")).toURI());
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(citiesFile));
            JSONArray cityList = (JSONArray) jsonObject.get("city_list");
            Iterator<JSONObject> iterator = (Iterator<JSONObject>) cityList.iterator();
            JSONArray weatherList = new JSONArray();
            while (iterator.hasNext()) {
                JSONObject obj = iterator.next();
                if (obj.get("city_name").equals(city)) {
                    weatherList = (JSONArray) obj.get("weather_list");
                    break;
                }
            }
            for (Object obj : weatherList) {
                JSONObject jsonObj = (JSONObject) obj;
                Weather weather = new Weather();
                weather.setHour((String) jsonObj.get("hour"));
                weather.setTemperature((String) jsonObj.get("temperature"));
                weather.setPressure((String) jsonObj.get("pressure"));
                weather.setRainChances((String) jsonObj.get("rain_chances"));
                weather24h.add(weather);
            }
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("Couldn't find the cities!");
        } catch (ParseException e) {
            System.out.println("Couldn't read the cities!");
        } catch (URISyntaxException e) {
            System.out.println("Something went wrong");
        }
    }

    private void writeWeather(PrintWriter out) {
        Queue<Weather> weatherQueue = new LinkedList<>(weather24h);
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH");

        while (!(weatherQueue.peek() != null && weatherQueue.peek().getHour().startsWith(timeFormatter.format(now)))) {
            Weather weather = weatherQueue.poll();
            weatherQueue.add(weather);
        }

        for (Weather weather : weatherQueue) {
            out.println("<tr>");
            out.println("<td style=\"text-align:center\">" + weather.getHour() + "</td>");
            out.println("<td style=\"text-align:center\">" + weather.getTemperature() + "</td>");
            out.println("<td style=\"text-align:center\">" + weather.getPressure() + "</td>");
            out.println("<td style=\"text-align:center\">" + weather.getRainChances() + "</td>");
            out.println("</tr>");
        }
    }
}
