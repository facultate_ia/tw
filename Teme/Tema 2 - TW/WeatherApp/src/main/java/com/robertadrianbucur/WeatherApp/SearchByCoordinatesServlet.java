package com.robertadrianbucur.WeatherApp;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SearchByCoordinatesServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String latitude = request.getParameter("latitude");
        String longitude = request.getParameter("longitude");

        response.setContentType("text/html");
        request.setAttribute("latitude", latitude);
        request.setAttribute("longitude", longitude);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/city");
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("search-by-coordinates.html");
        dispatcher.forward(request, response);
    }
}
