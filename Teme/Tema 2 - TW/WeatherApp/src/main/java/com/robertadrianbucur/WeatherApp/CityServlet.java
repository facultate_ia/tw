package com.robertadrianbucur.WeatherApp;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.robertadrianbucur.WeatherApp.Models.City;
import com.robertadrianbucur.WeatherApp.Models.Coordinates;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CityServlet extends HttpServlet {
    private final ArrayList<City> cities = new ArrayList<>();
    private ArrayList<City> matchedCities = new ArrayList<>();

    @Override
    public void init() {
        JSONParser parser = new JSONParser();
        try {
            File citiesFile = new File(Objects.requireNonNull(getClass().getClassLoader().getResource("weather.json")).toURI());
            JSONObject jsonObject = (JSONObject) parser.parse(new FileReader(citiesFile));
            JSONArray cityList = (JSONArray) jsonObject.get("city_list");
            Iterator<JSONObject> iterator = (Iterator<JSONObject>) cityList.iterator();
            while (iterator.hasNext()) {
                JSONObject obj = iterator.next();
                String name = (String) obj.get("city_name");
                JSONObject coordObj = (JSONObject) obj.get("coordinates");
                Coordinates coordinate = new Coordinates();
                coordinate.setLongitude((String) coordObj.get("longitude"));
                coordinate.setLatitude((String) coordObj.get("latitude"));
                City city = new City();
                city.setName(name);
                city.setCoordinates(coordinate);
                cities.add(city);
            }
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println("Couldn't find the cities!");
        } catch (ParseException e) {
            System.out.println("Couldn't read the cities!");
        } catch (URISyntaxException e) {
            System.out.println("Something went wrong");
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String cityName = (String) request.getAttribute("city");
        String longitude = (String) request.getAttribute("longitude");
        String latitude = (String) request.getAttribute("latitude");

        PrintWriter out = response.getWriter();
        out.println("<html><head><title>Cities</title></head><body>");
        out.println("<center><form method=\"post\" action=\"weather\">");
        if (cityName != null) {
            matchedCities = getMatchedCitiesByName(cityName);
        } else {
            matchedCities = getMatchedCitiesByCoordinates(longitude, latitude);
        }
        writeCities(out);
        out.println("<br/><br/><button name=\"see_weather\" type=\"see_weather\" class=\"btn btn-primary\">See weather</button>");
        out.println("</form><form method=\"post\" action=\"" + (cityName != null ? "search-by-name" : "search-by-coordinates") + "\">");
        out.println("<button name=\"back\" type=\"back\" class=\"btn btn-primary\">Back</button>");
        out.println("</form></center></body></html>");
    }

    private void writeCities(PrintWriter out) {
        out.println("<select required id=\"cities\" name=\"cities\" size=\"" + matchedCities.size() + "\">");
        for (City city : matchedCities) {
            out.println("<option value=\"" + city.getName() + "\">" + city.getName() + "</option>");
        }
        out.println("</select>");
    }

    private ArrayList<City> getMatchedCitiesByName(String searchedCity) {
        ArrayList<City> cityList = new ArrayList<>();

        for (City city : cities) {
            if (city.getName().toLowerCase(Locale.ROOT).contains(searchedCity.toLowerCase(Locale.ROOT))) {
                cityList.add(city);
            }
        }

        return cityList;
    }

    private ArrayList<City> getMatchedCitiesByCoordinates(String longitude, String latitude) {
        ArrayList<City> cityList = new ArrayList<>();

        for (City city : cities) {
            if (city.getCoordinates().getLatitude().toLowerCase(Locale.ROOT).contains(latitude.toLowerCase(Locale.ROOT)) &&
                    city.getCoordinates().getLongitude().toLowerCase(Locale.ROOT).contains(longitude.toLowerCase(Locale.ROOT))) {
                cityList.add(city);
            }
        }

        return cityList;
    }
}
