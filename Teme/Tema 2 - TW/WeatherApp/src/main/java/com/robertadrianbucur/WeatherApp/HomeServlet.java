package com.robertadrianbucur.WeatherApp;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeServlet extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String cityButton = request.getParameter("city-button");
        String coordinatesButton = request.getParameter("coordinates-button");

        if (cityButton != null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/search-by-name");
            dispatcher.forward(request, response);
        } else if (coordinatesButton != null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/search-by-coordinates");
            dispatcher.forward(request, response);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.html");
        dispatcher.forward(request, response);
    }
}