package com.robertadrianbucur.WeatherApp.Models;

public class Weather {
    private String hour;
    private String temperature;
    private String pressure;
    private String rainChances;

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getRainChances() {
        return rainChances;
    }

    public void setRainChances(String rainChances) {
        this.rainChances = rainChances;
    }
}
