/**
 * The main class that starts the client.
 *
 * Created by Robert-Adrian Bucur
 */
public class Main {

    /**
     * Runs the client.
     */
    public static void main(String[] args) {
        String hostname = args.length < 2 ? "localhost" : args[0];
        int port = args.length < 2 ? 9900 : Integer.parseInt(args[1]);

        ClientThread clientThread = new ClientThread(hostname, port);
        clientThread.start();
    }
}
