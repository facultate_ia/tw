import java.io.BufferedReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * The thread that runs the client.
 *
 * Created by Robert-Adrian Bucur
 */
public class ClientThread extends Thread {
    private final List<String> otherUsers = new ArrayList<>();
    private final List<String> subscribedTopics = new ArrayList<>();
    private final List<String> topics = new ArrayList<>();
    private PrintWriter writer;
    private BufferedReader reader;
    private Scanner scanner;
    private Socket socket;

    /**
     * Constructor for initializing the client to listen to a specific port on a specific host.
     *
     * @param  hostname  the port number
     * @param  port  the server
     */
    public ClientThread(String hostname, int port) {
        try {
            socket = new Socket(hostname, port);
            OutputStream output = socket.getOutputStream();
            InputStream input = socket.getInputStream();
            reader = new BufferedReader(new InputStreamReader(input));
            writer = new PrintWriter(output, true);
            scanner = new Scanner(System.in);
        } catch (IOException ex) {
            System.out.println("Error getting input stream: " + ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Implementation of the run method from Thread.
     *
     * Calls the methods to realize the exchange of data with the server.
     */
    @Override
    public void run() {
        try {
            System.out.println("Connected to the server");

            getOtherUsers();

            enterUserName();

            getTopicsFromServer();

            printAllTopics();

            chooseTopics();

            printSubscribedTopics();

            new ReadFromServerThread(socket).start();

            System.out.println("To send a message write [subscribedTopic]: [message] \nFor example: \"sport: Hello sport's fans!\"");
            System.out.println("If you want to leave just say \"bye\"");

            writeToServer();

            closeConnections();
        } catch (UnknownHostException ex) {
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O Error: " + ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Receives the users list from the server.
     */
    private void getOtherUsers() throws IOException {
        String user;
        do {
            user = reader.readLine();
            if (!user.equals("/0")) {
                otherUsers.add(user);
            }
        } while (!user.equals("/0"));
    }

    /**
     * Requests the user to enter a username.
     */
    private void enterUserName() {
        boolean userIsInList = true;
        String userName = "";
        while (userIsInList) {
            userIsInList = false;
            System.out.println("\nEnter your name: ");
            userName = scanner.nextLine();
            for (String otherUser : otherUsers) {
                if (userName.equals(otherUser)) {
                    userIsInList = true;
                    System.out.println("The username is already taken!");
                    break;
                }
            }
        }
        if (!socket.isClosed()) {
            writer.println(userName);
        }
    }

    /**
     * Receives the topics list from the server.
     */
    private void getTopicsFromServer() throws IOException {
        String topic;
        do {
            topic = reader.readLine();
            if (!topic.equals("/0")) {
                topics.add(topic);
            }
        } while (!topic.equals("/0"));
    }

    /**
     * Prints to the console all topics.
     */
    private void printAllTopics() {
        StringBuilder message = new StringBuilder("The topics are:\n");
        int index = 1;
        for (String topic : topics) {
            message.append(index).append(". ").append(topic).append("\n");
            index++;
        }
        System.out.println(message);
    }

    /**
     * Requests the user to choose to subscribed to some topics.
     */
    private void chooseTopics() {
        System.out.println("Choose Topic Mode: ON \nPress 0 to exit Choose Topic Mode!");
        int indexTopic;
        do {
            System.out.println("Subscribe to a topic by entering its number: ");
            indexTopic = Integer.parseInt(scanner.nextLine());
            if (indexTopic > 0 && indexTopic < topics.size()) {
                subscribedTopics.add(topics.get(indexTopic - 1));
            } else if (indexTopic != 0) {
                System.out.println("Choose a valid topic!");
            }
            writer.println(indexTopic);
        } while (indexTopic != 0);
    }

    /**
     * Prints to the console the topics that the user subscribed to.
     */
    private void printSubscribedTopics() {
        StringBuilder message = new StringBuilder("You have subscribed to these topics:\n");
        for (String subscribedTopic : subscribedTopics) {
            message.append(subscribedTopic).append("\n");
        }
        System.out.println(message);
    }

    /**
     * Sends the messages to the server.
     */
    private void writeToServer() {
        String text = "";

        while (!text.contains("bye") && !socket.isClosed()) {
            text = scanner.nextLine();
            if (text.toLowerCase().contains("bye")) {
                return;
            }
            if (text.contains(":")) {
                String selectedTopic = text.split(":")[0];
                boolean isValidTopic = false;
                for (String topic : subscribedTopics) {
                    if (selectedTopic.toLowerCase().contains(topic.toLowerCase())) {
                        if (!socket.isClosed()) {
                            writer.println(text);
                            isValidTopic = true;
                        }
                    }
                }
                if (!isValidTopic) {
                    System.out.println("You have chosen an unsubscribed topic!");
                }
            } else {
                System.out.println("The message is invalid!");
            }
        }
    }

    /**
     * Close all connections.
     */
    private void closeConnections() throws IOException {
        scanner.close();
        reader.close();
        writer.close();
        socket.close();
    }
}