import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * The thread for reading the messages from the server
 *
 * Created by Robert-Adrian Bucur
 */
public class ReadFromServerThread extends Thread {
    private BufferedReader reader;
    private final Socket socket;

    /**
     * Constructor for initializing the thread to connect to a specific socket.
     *
     * @param  socket  the socket
     */
    public ReadFromServerThread(Socket socket) {
        this.socket = socket;

        try {
            InputStream input = socket.getInputStream();
            reader = new BufferedReader(new InputStreamReader(input));
        } catch (IOException ex) {
            System.out.println("Error getting input stream: " + ex.getMessage());
            ex.printStackTrace();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Implementation of the run method from Thread.
     *
     * Reads the response from the server and displays it concurrently
     */
    @Override
    public void run() {
        try {
            while (!socket.isClosed()) {
                String response = reader.readLine();
                if (response == null) {
                    socket.close();
                    break;
                }
                System.out.println("\n" + response);
            }
            System.out.println("The server is shut down!");
            System.exit(130);
        } catch (IOException ex) {
            System.out.println("Some error occurred! " + ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
