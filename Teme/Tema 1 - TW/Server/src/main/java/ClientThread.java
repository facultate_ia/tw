import java.net.Socket;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;

/**
 * The threads that run for every client.
 *
 * Created by Robert-Adrian Bucur
 */
public class ClientThread extends Thread {
    private final Socket socket;
    private final ServerThread serverThread;
    private PrintWriter writer;
    private String userName;
    private final List<String> subscribedTopics = new ArrayList<>();
    private BufferedReader reader;

    /**
     * Constructor for initializing the server to listen to a specific port.
     *
     * @param  socket  the port number
     * @param  serverThread  the server
     */
    public ClientThread(Socket socket, ServerThread serverThread) {
        this.socket = socket;
        this.serverThread = serverThread;
    }

    /**
     * Implementation of the run method from Thread.
     *
     * Creates a server socket and listen to it.
     * When a new client connects, it runs it in a new thread.
     */
    @Override
    public void run() {
        try {
            InputStream input = socket.getInputStream();
            reader = new BufferedReader(new InputStreamReader(input));

            OutputStream output = socket.getOutputStream();
            writer = new PrintWriter(output, true);

            printUsers();

            String temp = reader.readLine();
            if (temp == null) {
                return;
            }
            userName = temp;
            serverThread.addUserName(userName);

            printTopics();

            getSubscribedTopicsFromClient();

            chatWithClient();

            serverThread.removeUser(userName, this);
            socket.close();
        } catch (IOException ex) {
            System.out.println("Error in UserThread: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Prints the users to the client.
     */
    private void printUsers() {
        for (String user : serverThread.getUserNames()) {
            writer.println(user);
        }
        writer.println("/0");
    }

    /**
     * Prints the topics to the client.
     */
    private void printTopics() {
        for (String topic : serverThread.getTopics()) {
            writer.println(topic);
        }
        writer.println("/0");
    }

    /**
     * Gets the topics that the client subscribed to.
     */
    private void getSubscribedTopicsFromClient() throws IOException {
        int indexTopic;
        do {
            String temp = reader.readLine();
            if (temp == null) {
                break;
            }
            indexTopic = Integer.parseInt(temp);
            if (indexTopic != 0) {
                subscribedTopics.add(serverThread.getTopics().get(indexTopic - 1));
            }
        } while (indexTopic != 0);
    }

    /**
     * Realizes the exchange of messages with the client.
     */
    private void chatWithClient() throws IOException {
        String serverMessage;
        String clientMessage;

        do {
            clientMessage = reader.readLine();
            if (clientMessage == null) {
                break;
            }
            String selectedTopic = clientMessage.split(":")[0];
            serverMessage = "[" + userName + "] on " + clientMessage;
            serverThread.broadcast(serverMessage, this, selectedTopic);
            if (clientMessage.contains("bye")) {
                break;
            }
        } while (serverThread.hasUsers());
    }

    /**
     * Sends the massage to the client.
     */
    public void sendMessage(String message) {
        writer.println(message);
    }

    /**
     * Returns the subscribed topics list.
     */
    public List<String> getSubscribedTopics() {
        return subscribedTopics;
    }
}