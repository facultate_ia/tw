import java.net.ServerSocket;
import java.net.Socket;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The thread that the server runs on.
 *
 * Created by Robert-Adrian Bucur
 */
public class ServerThread extends Thread{
    private final int port;
    private final List<String> userNames = new ArrayList<>();
    private final List<ClientThread> clientThreads = new ArrayList<>();
    private final List<String> topics = new ArrayList<>();

    /**
     * Constructor for initializing the server to listen to a specific port.
     *
     * @param  port  the port number
     */
    public ServerThread(int port) {
        this.port = port;
        generateTopics();
    }

    /**
     * Implementation of the run method from Thread.
     *
     * Creates a server socket and listen to it.
     * When a new client connects, it runs it in a new thread.
     */
    @Override
    public void run() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {

            System.out.println("The server is listening to port " + port);

            while (!serverSocket.isClosed()) {
                Socket socket = serverSocket.accept();
                System.out.println("New user connected");

                ClientThread newUser = new ClientThread(socket, this);
                clientThreads.add(newUser);
                newUser.start();
            }

        } catch (IOException ex) {
            System.out.println("Error in the server: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * Generates the topics list.
     */
    private void generateTopics() {
        topics.add("Sport");
        topics.add("Cuisine");
        topics.add("Movies");
        topics.add("Books");
    }

    /**
     * Sends the message to all the clients that are subscribed to a specific topic.
     *
     * @param  message  the message to be sent
     * @param  excludeUser  the user how sent the message (he will not receive the message)
     * @param  selectedTopic  the topic to which the user sent the message
     */
    public void broadcast(String message, ClientThread excludeUser, String selectedTopic) {
        for (ClientThread user : clientThreads) {
            if (user != excludeUser) {
                for (String subscribedTopic : user.getSubscribedTopics()) {
                    if (subscribedTopic.toLowerCase().contains(selectedTopic.toLowerCase()) || selectedTopic.toLowerCase().contains(subscribedTopic.toLowerCase())) {
                        user.sendMessage(message);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Gets the list of users.
     */
    public List<String> getUserNames() {
        return this.userNames;
    }

    /**
     * Adds a new user to the list.
     *
     * @param  userName  the user name to be added
     */
    public void addUserName(String userName) {
        userNames.add(userName);
    }

    /**
     * Remove a user from the list when he disconnect.
     *
     * @param  userName  the users name
     * @param  clientThread  the client how quited
     */
    public void removeUser(String userName, ClientThread clientThread) {
        boolean removed = userNames.remove(userName);
        if (removed) {
            clientThreads.remove(clientThread);
            System.out.println("User " + (userName != null ? userName : "NO NAME") + " quited");
        }
    }

    /**
     * Verifies if the server has users connected.
     */
    public boolean hasUsers() {
        return !this.userNames.isEmpty();
    }

    /**
     * Returns the list of topics.
     */
    public List<String> getTopics() {
        return this.topics;
    }
}
