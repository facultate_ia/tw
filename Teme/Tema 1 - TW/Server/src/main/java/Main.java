/**
 * The main class that starts the server.
 *
 * Created by Robert-Adrian Bucur
 */
public class Main {

    /**
     * Runs the server.
     */
    public static void main(String[] args) {
        int port = args.length < 1 ? 9900 : Integer.parseInt(args[0]);

        ServerThread serverThread = new ServerThread(port);
        serverThread.start();
    }
}
