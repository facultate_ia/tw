package com.robertadrianbucur.vacation_manager.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "vacation_request")
public class VacationRequest {
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private Employee employee;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JoinTable(name = "request_chief", joinColumns = {@JoinColumn(name = "request_id")},
            inverseJoinColumns = {@JoinColumn(name = "chief_id")})
    private List<Chief> chiefs;

    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "end_date", nullable = false)
    private Date endDate;

    public VacationRequest() {
    }

    public VacationRequest(Employee employee, List<Chief> chiefs, Date startDate, Date endDate) {
        this.employee = employee;
        this.chiefs = chiefs;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public List<Chief> getChiefs() {
        return chiefs;
    }

    public void setChiefs(List<Chief> chiefs) {
        this.chiefs = chiefs;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "VacationRequest{" +
                "id=" + id +
                ", employee='" + employee + "'" +
                ", chiefs='" + chiefs + "'" +
                ", start_date='" + startDate.toString() + "'" +
                ", end_date='" + endDate.toString() + "'" +
                "}";
    }
}
