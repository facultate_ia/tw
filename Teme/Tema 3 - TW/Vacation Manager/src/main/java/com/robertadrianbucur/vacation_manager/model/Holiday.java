package com.robertadrianbucur.vacation_manager.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "holiday")
public class Holiday {
    @Id
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "holiday_date", nullable = false)
    private Date holidayDate;

    public Holiday() {
    }

    public Holiday(int id, Date holidayDate) {
        this.id = id;
        this.holidayDate = holidayDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getHolidayDate() {
        return holidayDate;
    }

    public void setHolidayDate(Date holidayDate) {
        this.holidayDate = holidayDate;
    }

    @Override
    public String toString() {
        return "Holiday{" +
                "id=" + id +
                ", holidayDate=" + holidayDate +
                '}';
    }
}
