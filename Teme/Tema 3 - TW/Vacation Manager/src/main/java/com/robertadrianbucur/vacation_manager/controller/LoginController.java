package com.robertadrianbucur.vacation_manager.controller;

import com.robertadrianbucur.vacation_manager.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController {
    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/login")
    public String login(String email, String password, Model model) {
        if (employeeService.loginEmployee(email, password)) {
            return "redirected:menu";
        } else {
            model.addAttribute("hasError", true);
            return "login";
        }
    }

    @GetMapping("/login")
    public String getLoginView(){
        return "login";
    }
}
