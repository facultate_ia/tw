package com.robertadrianbucur.vacation_manager.service;

import com.robertadrianbucur.vacation_manager.model.Chief;
import com.robertadrianbucur.vacation_manager.repository.ChiefRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.List;

@Service
public class ChiefService {
    @Autowired
    private ChiefRepository chiefRepository;

    public boolean loginChief(String email, String password) {
        Optional<Chief> chiefOptional = chiefRepository.findByEmail(email);
        if (chiefOptional.isPresent()) {
            if (chiefOptional.get().getPassword() == password) {
                return true;
            }
        }
        return false;
    }

    public Chief getCeo() {
        List<Chief> chiefs = chiefRepository.findAll();
        for (Chief chief : chiefs) {
            if (chief.isCeo()) {
                return chief;
            }
        }
        return null;
    }
}
