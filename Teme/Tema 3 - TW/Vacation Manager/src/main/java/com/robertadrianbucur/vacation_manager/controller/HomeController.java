package com.robertadrianbucur.vacation_manager.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/add-vacation-request")
    public String addVacationRequestView(){
        return "add-vacation-request";
    }

    @GetMapping("/see-vacation-requests")
    public String getVacationRequestView(){
        return "see-vacation-requests";
    }
}
