package com.robertadrianbucur.vacation_manager.controller;

import com.robertadrianbucur.vacation_manager.service.VacationRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Date;

@Controller
public class VacationRequestController {
    @Autowired
    private VacationRequestService vacationRequestService;

    @PostMapping("/add-vacation-request")
    public String addVacationRequest(String email, Date startDate, Date endDate) {
        vacationRequestService.addVacationRequest(email, startDate, endDate);
        return "home";
    }

    @GetMapping("/see-vacation-requests")
    public String seeVacationRequests(String email) {
        vacationRequestService.getVacationRequests(email);
        return "home";
    }
}
