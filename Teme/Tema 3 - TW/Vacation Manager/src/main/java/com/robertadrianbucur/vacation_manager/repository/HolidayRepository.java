package com.robertadrianbucur.vacation_manager.repository;

import com.robertadrianbucur.vacation_manager.model.Holiday;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface HolidayRepository extends JpaRepository<Holiday, Integer> {
    List<Holiday> findAllByHolidayDate(Date holidayDate);
}