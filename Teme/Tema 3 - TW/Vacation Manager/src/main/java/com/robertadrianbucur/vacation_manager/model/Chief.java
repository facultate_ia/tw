package com.robertadrianbucur.vacation_manager.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "chief")
public class Chief {
    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "isCeo", nullable = false)
    private boolean isCeo;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "days_left", nullable = false)
    private int daysLeft;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JoinTable(name = "employee_team", joinColumns =
            {@JoinColumn(name = "employee_id")}, inverseJoinColumns = {@JoinColumn(name = "team_id")})
    private Set<Team> teams;

    public Chief() {
    }

    public Chief(int id, String name, boolean isCeo, String email, String password, int daysLeft) {
        this.id = id;
        this.name = name;
        this.isCeo = isCeo;
        this.email = email;
        this.password = password;
        this.daysLeft = daysLeft;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCeo() {
        return isCeo;
    }

    public void setCeo(boolean ceo) {
        isCeo = ceo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(int daysLeft) {
        this.daysLeft = daysLeft;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    @Override
    public String toString() {
        return "Chief{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", isCeo=" + isCeo +
                ", email='" + email + "'" +
                ", password='" + password + "'" +
                ", daysLeft=" + daysLeft +
                ", teams=" + teams +
                '}';
    }
}
