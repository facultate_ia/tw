package com.robertadrianbucur.vacation_manager.repository;

import com.robertadrianbucur.vacation_manager.model.Chief;
import com.robertadrianbucur.vacation_manager.model.Employee;
import com.robertadrianbucur.vacation_manager.model.Team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer> {
    Optional<Team> findByName(String name);
    List<Team> findAllByChief(Chief chief);
    List<Team> findAllByEmployee(Employee employee);
}
