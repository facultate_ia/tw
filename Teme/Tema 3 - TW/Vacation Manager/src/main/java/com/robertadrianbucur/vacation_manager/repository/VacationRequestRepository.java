package com.robertadrianbucur.vacation_manager.repository;

import com.robertadrianbucur.vacation_manager.model.Employee;
import com.robertadrianbucur.vacation_manager.model.VacationRequest;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VacationRequestRepository extends JpaRepository<VacationRequest, Integer> {
    List<VacationRequest> findAllByEmployee(Employee employee);
    VacationRequest save(VacationRequest vacationRequest);
}
