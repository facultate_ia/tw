package com.robertadrianbucur.vacation_manager.service;

import com.robertadrianbucur.vacation_manager.model.Employee;
import com.robertadrianbucur.vacation_manager.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public boolean loginEmployee(String email, String password) {
        Optional<Employee> employeeOptional = employeeRepository.findByEmail(email);
        if (employeeOptional.isPresent()) {
            Employee employee = employeeOptional.get();
            if (employee.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public boolean getDaysLeft(Employee employee, int numberOfDays) {
        Optional<Employee> employeeOptional = employeeRepository.findByEmail(employee.getEmail());
        if (employeeOptional.isPresent()) {
            if (employeeOptional.get().getDaysLeft() > numberOfDays) {
                return true;
            }
        }
        return false;
    }
}
