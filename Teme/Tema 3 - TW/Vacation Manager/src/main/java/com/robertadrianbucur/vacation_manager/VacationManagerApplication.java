package com.robertadrianbucur.vacation_manager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.robertadrianbucur.vacation_manager.repository"})
public class VacationManagerApplication {
    public static void main(String[] args) {
        SpringApplication.run(VacationManagerApplication.class, args);
    }
}
