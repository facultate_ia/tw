package com.robertadrianbucur.vacation_manager.repository;

import com.robertadrianbucur.vacation_manager.model.Chief;
import com.robertadrianbucur.vacation_manager.model.Employee;
import com.robertadrianbucur.vacation_manager.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChiefRepository extends JpaRepository<Chief, Integer> {
    @Override
    List<Chief> findAll();
    Optional<Chief> findByEmail(String email);
    Optional<Chief> findByTeam(Team team);
    List<Chief> findAllByEmployee(Employee employee);
}