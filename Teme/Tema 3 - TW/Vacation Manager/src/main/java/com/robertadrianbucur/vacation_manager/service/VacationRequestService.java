package com.robertadrianbucur.vacation_manager.service;

import com.robertadrianbucur.vacation_manager.model.Chief;
import com.robertadrianbucur.vacation_manager.model.Employee;
import com.robertadrianbucur.vacation_manager.model.VacationRequest;
import com.robertadrianbucur.vacation_manager.repository.ChiefRepository;
import com.robertadrianbucur.vacation_manager.repository.EmployeeRepository;
import com.robertadrianbucur.vacation_manager.repository.VacationRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import java.util.Set;

@Service
public class VacationRequestService {
    @Autowired
    private VacationRequestRepository vacationRequestRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ChiefRepository chiefRepository;

    public List<VacationRequest> getVacationRequests(String email) {
        Optional<Employee> employeeOptional = employeeRepository.findByEmail(email);
        List<VacationRequest> vacationRequests = new ArrayList<VacationRequest>();
        if (employeeOptional.isPresent()) {
            vacationRequests = vacationRequestRepository.findAllByEmployee(employeeOptional.get());
        }
        return vacationRequests;
    }

    public void addVacationRequest(String email, Date startDate, Date endDate) {
        Optional<Employee> employeeOptional = employeeRepository.findByEmail(email);
        if (employeeOptional.isPresent()) {
            List<Chief> chiefs = chiefRepository.findAllByEmployee(employeeOptional.get());
            VacationRequest vacationRequest = new VacationRequest(employeeOptional.get(), chiefs, startDate, endDate);
            vacationRequestRepository.save(vacationRequest);
        }
    }
}
