package com.robertadrianbucur.vacation_manager.service;

import com.robertadrianbucur.vacation_manager.model.Chief;
import com.robertadrianbucur.vacation_manager.model.Team;
import com.robertadrianbucur.vacation_manager.repository.ChiefRepository;
import com.robertadrianbucur.vacation_manager.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private ChiefRepository chiefRepository;

    public List<Team> getTeamsLeadByChief(String email) {
        Optional<Chief> chiefOptional = chiefRepository.findByEmail(email);
        List<Team> teams = new ArrayList<Team>();
        if (chiefOptional.isPresent()) {
            teams = teamRepository.findAllByChief(chiefOptional.get());
        }
        return teams;
    }

    public Team getTeamByName(String name) {
        Optional<Team> teamOptional = teamRepository.findByName(name);
        if (teamOptional.isPresent()) {
            return teamOptional.get();
        }
        return null;
    }
}
