package com.robertadrianbucur.examen_tw.services;

import com.robertadrianbucur.examen_tw.dtos.GameDto;
import com.robertadrianbucur.examen_tw.models.Game;
import com.robertadrianbucur.examen_tw.models.Team;
import com.robertadrianbucur.examen_tw.repositories.GameRepository;
import com.robertadrianbucur.examen_tw.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class GameService {
    private GameRepository gameRepository;
    private TeamRepository teamRepository;

    public GameService(@Autowired GameRepository gameRepository, @Autowired TeamRepository teamRepository) {
        this.gameRepository = gameRepository;
        this.teamRepository = teamRepository;
    }

    private boolean isTournamentFinished(List<Team> teams) {
        int numberOfWinners = 0;
        for (Team team : teams) {
            if (team.getState() == "WINNER") {
                numberOfWinners += 1;
            }
        }
        if (numberOfWinners == 1) {
            return true;
        }
        return false;
    }

    private GameDto generateGame() {
        Random random = new Random();
        List<Team> teams = teamRepository.findAll();
        if (isTournamentFinished(teams) == true) {
            return null;
        }
        Team team1 = teams.get(random.nextInt(teams.size()));
        while (team1.getState() == "LOSER") {
            team1 = teams.get(random.nextInt(teams.size()));
        }
        Team team2 = teams.get(random.nextInt(teams.size()));
        while (team1.getId() == team2.getId() || team2.getState() == "LOSER") {
            team2 = teams.get(random.nextInt(teams.size()));
        }
        team1.setState("PENDING");
        team2.setState("PENDING");

        return new GameDto("LEVEL 1", "0", "0", team1, team2);
    }

    public List<GameDto> getGames() {
        return new ArrayList<GameDto>(Arrays.asList(generateGame()));
    }

    public Game saveGame(GameDto gameDto) {
        Game game = new Game();
        game.setLevel("LEVEL 1");
        game.setScoreTeam1(gameDto.getScoreTeam1());
        game.setScoreTeam2(gameDto.getScoreTeam2());
        return gameRepository.save(game);
    }
}
