package com.robertadrianbucur.examen_tw.controllers;

import com.robertadrianbucur.examen_tw.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TeamController {
    private TeamService teamService;

    public TeamController(@Autowired TeamService teamService) {
        this.teamService = teamService;
    }

    @GetMapping("/teams")
    public String getTeamsPage(Model model) {
        model.addAttribute("teams", teamService.getAllTeams());
        return "teams";
    }
}
