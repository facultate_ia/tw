package com.robertadrianbucur.examen_tw.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping("")
    public String getIndex() {
        return "index";
    }

    @RequestMapping("/index")
    public String getIndexPage() {
        return "index";
    }
}
