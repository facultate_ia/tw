package com.robertadrianbucur.examen_tw.dtos;

import com.robertadrianbucur.examen_tw.models.Team;

public class GameDto {
    private String level;
    private String scoreTeam1;
    private String scoreTeam2;
    private Team team1;
    private Team team2;

    public GameDto() {
    }

    public GameDto(String level, String scoreTeam1, String scoreTeam2, Team team1, Team team2) {
        this.level = level;
        this.scoreTeam1 = scoreTeam1;
        this.scoreTeam2 = scoreTeam2;
        this.team1 = team1;
        this.team2 = team2;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getScoreTeam1() {
        return scoreTeam1;
    }

    public void setScoreTeam1(String scoreTeam1) {
        this.scoreTeam1 = scoreTeam1;
    }

    public String getScoreTeam2() {
        return scoreTeam2;
    }

    public void setScoreTeam2(String scoreTeam2) {
        this.scoreTeam2 = scoreTeam2;
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }
}
