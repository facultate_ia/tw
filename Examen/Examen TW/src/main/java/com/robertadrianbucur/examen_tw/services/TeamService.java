package com.robertadrianbucur.examen_tw.services;

import com.robertadrianbucur.examen_tw.dtos.GameDto;
import com.robertadrianbucur.examen_tw.dtos.TeamDto;
import com.robertadrianbucur.examen_tw.models.Game;
import com.robertadrianbucur.examen_tw.models.Team;
import com.robertadrianbucur.examen_tw.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class TeamService {
    private TeamRepository teamRepository;

    public TeamService(@Autowired TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    public List<TeamDto> getAllTeams() {
        List<Team> teams = teamRepository.findAll();
        List<TeamDto> teamDtos = new ArrayList<TeamDto>();
        for (Team team : teams) {
            teamDtos.add(new TeamDto(team.getTeamName(), team.getState()));
        }
        return teamDtos;
    }
}
