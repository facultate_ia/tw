package com.robertadrianbucur.examen_tw.repositories;

import com.robertadrianbucur.examen_tw.models.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GameRepository extends JpaRepository<Game, Integer> {
    @Override
    List<Game> findAll();
}
