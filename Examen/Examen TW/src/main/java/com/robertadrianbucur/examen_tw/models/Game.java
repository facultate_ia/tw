package com.robertadrianbucur.examen_tw.models;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "game")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private int id;

    @Column(name = "level", nullable = false)
    private String level;

    @Column(name = "score_team1", nullable = false)
    private String scoreTeam1;

    @Column(name = "score_team2", nullable = false)
    private String scoreTeam2;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.REMOVE})
    @JoinTable(name = "team_game", joinColumns = {@JoinColumn(name = "game_id")},
    inverseJoinColumns = {@JoinColumn(name = "team_id")})
    private Set<Team> teams;

    public Game() {
    }

    public Game(int id, String level, String scoreTeam1, String scoreTeam2, Set<Team> teams) {
        this.id = id;
        this.level = level;
        this.scoreTeam1 = scoreTeam1;
        this.scoreTeam2 = scoreTeam2;
        this.teams = teams;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getScoreTeam1() {
        return scoreTeam1;
    }

    public void setScoreTeam1(String scoreTeam1) {
        this.scoreTeam1 = scoreTeam1;
    }

    public String getScoreTeam2() {
        return scoreTeam2;
    }

    public void setScoreTeam2(String scoreTeam2) {
        this.scoreTeam2 = scoreTeam2;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", level=" + level +
                ", scoreTeam1='" + scoreTeam1 + '\'' +
                ", scoreTeam2='" + scoreTeam2 + '\'' +
                ", teams=" + teams +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return teams.equals(game.teams);
    }

    @Override
    public int hashCode() {
        return Objects.hash(teams);
    }
}
