package com.robertadrianbucur.examen_tw.controllers;

import com.robertadrianbucur.examen_tw.dtos.GameDto;
import com.robertadrianbucur.examen_tw.services.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class GameController {
    private GameService gameService;

    public GameController(@Autowired GameService gameService) {
        this.gameService = gameService;
    }

    @ModelAttribute("game")
    public GameDto gameDto() {
        return new GameDto();
    }

    @GetMapping("/games")
    public String getGamesPage(Model model) {
        model.addAttribute("game", gameService.getGames().get(0));
        return "games";
    }

    @PostMapping("/game_saved")
    public String saveGame(@ModelAttribute("game") GameDto gameDto) {
        gameService.saveGame(gameDto);
        return "game_saved";
    }
}
