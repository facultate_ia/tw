package com.robertadrianbucur.examen_tw.dtos;

import com.robertadrianbucur.examen_tw.models.Game;

import java.util.Set;

public class TeamDto {
    private String teamName;
    private String state;
    private Set<Game> games;

    public TeamDto() {
    }

    public TeamDto(String teamName, String state, Set<Game> games) {
        this.teamName = teamName;
        this.state = state;
        this.games = games;
    }

    public TeamDto(String teamName, String state) {
        this.teamName = teamName;
        this.state = state;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Set<Game> getGames() {
        return games;
    }

    public void setGames(Set<Game> games) {
        this.games = games;
    }
}
