package com.robertadrianbucur.examen_tw.repositories;

import com.robertadrianbucur.examen_tw.models.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer> {
    @Override
    List<Team> findAll();
}
